const user = {
    name: "John",
    contact: [
                {
                  countryCode: "880",
                  phone: "123456789"
                },
                {
                  countryCode: "880",
                  phone: "123456789"
                }
             ]
    };

    const hasDeep = require("has-deep-prop");


function printSecondary(user){
    const secondaryContact = hasDeep(user, "contact.1.phone");
    if(secondaryContact){
        console.log(secondaryContact);
      }
    else {
        console.log("No secondary contact");
     }
}

printSecondary(user)