const express = require('express');
const cookieParser = require('cookie-parser');
const app = express()

app.use(cookieParser());
app.use(express.json())
app.get("/transfer", (req, res) => {

    res.send(`
    <html>
      <form id="myForm" action="/transfer" method="POST" target="_blank">
        Account:<input type="text" name="account" value="your friend"/><br/>
        Amount :<input type="text" name="amount" value="$5000"/>
                <button>Transfer Money</button>
      </form>
    </html>
    `)
});

app.post("/transfer", (req, res) => {
    if (isAuthenticated(req.cookies["session"])) {
        // Transfer money and insert data in the database
        console.info("Transferring Money...")
        res.send("OK")
    } else {
        res.status(400).send("Bad Request")
    }
});

app.listen(4002);

const isAuthenticated = (session) => {
    // We should check session in a store or something like that
    return (session === "valid_user")
}