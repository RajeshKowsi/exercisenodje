const childProcess = require('child_process')
const express = require('express')
const bodyParser = require('body-parser')

const app = express();
app.use(bodyParser.json());


app.post("/someBigProcess", (req,res) => {
    const forked_child_process = childProcess.fork('./checkIfPrime.js');
    // send message to the child process

var val = "1111111111"
    forked_child_process.send(parseInt(val));
    // listen for a response from the child process
    forked_child_process.on("message", isTheNumberPrime => res.send(isTheNumberPrime));
})

app.listen(3000, () => {
    console.log('server started on port 3000');
})


//https://betterprogramming.pub/scaling-node-js-applications-with-multiprocessing-b0c25511832a

//autocannon -c 8 -a 8 -t 300 -m 'POST' -b '{"number":"1111111}' -H 'Content-Type:application/json' http://localhost:3000/someBigProcess


//autocannon -c 8 -a 8 -t 300 -m 'GET' -b '{"number":"1111111}' -H 'Content-Type:application/json' https://www.okdollar.co/RestService.svc/MonitorTransactionByStatus?Status=0&TransactionType=TOPUP


//https://www.okdollar.co/RestService.svc/MonitorTransactionByStatus?Status=0&TransactionType=TOPUP