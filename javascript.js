//Class:
//  Class declared with a name which acts as its identifier. We can use the name to create new objects using keyword new


// arr = ["BB", "CC", "AA", "BB", "CC", "DD"]

// countValues = arr.reduce((obje, arrs) => {

//     if (obje[arrs] == undefined) {
//         obje[arrs] = 1
//         return
//     } else {
//         obje[arrs]++
//         return
//     }

// }, {})

// console.log(countValues)

//===================================================================================================================

// - To validate interger on Not
// const value = 10.8
// console.log(!isNaN(value))

//==================================================================================================================

// let person = {
//     name : "Tutorial"
// }
//  Object.freeze(person)
//  person.name = "HI"

//  console.log(person)

 //outPut: { name: 'Tutorial' }
 // It wont give error but the value cant change

//=============================================================================================================

  // -- Remove duplication and provide unique values
// const Ids = [3,5,1,33,4,2,3,5,6,1]

// const uniqueValues =  Ids.filter((ele, index, arr) => arr.indexOf(ele) == index)

// console.log(uniqueValues)

//===================================================================================================================
  // -- Remove duplication and provide unique values with sorted

//   const Ids =  [3,5,1,33,4,2,3,5,6,1]
   
//   const sortedUniqueValues = Ids.filter( (ele, index, arr) => arr.indexOf(ele) == index).sort((a,b) =>
//   {
//     return  a-b
//   })

//   console.log(sortedUniqueValues)
  //===================================================================================================================

    //-  find the Max number in Array list

    // const Ids =  [3,5,1,33,4,2,3,5,6,1]

    // const maxNumber = Ids.reduce((pre, curr) => {
    //     return pre >curr? pre :curr
    // })
    
    // console.log(maxNumber)

    //====================================================================================================================
    // - How to find the average of the numbers in the numbered array

    // const studentId  = [59, 53,23,1, 67,84, 10]
     
    //  const total = studentId.reduce((a,b) =>a+b)
    //  console.log(total)

    //  const average = total/ studentId.length
    //  console.log(average)

     //====================================================================================================================

     // - How upper case the first charater in String?

    //   const days = ["sunday", "monday", "thusday", "wednesday", "thursday"]

    //   let UpdatedDays =[]

    //  for(let day of days){
    //      day = day.charAt(0).toUpperCase() + day.substring(1)

    //      UpdatedDays.push(day)
    //  }

    //  console.log(UpdatedDays)

    // ============================================================================================================================


       
    //-  find the Min number in Array list

    // const Ids =  [3,5,1,33,4,2,3,5,6,1]

    // const maxNumber = Ids.reduce((pre, curr) => {
    //     return pre <curr? pre :curr
    // })
    
    // console.log(maxNumber)

    //===============================================================================================================================

    //- How to make the sentence out the given array

    //   const elemetofarray = ["welcome", "to" ,"ARC", "tutorial"]
 
    //   const finalOutPut = elemetofarray.join(" ")

    //   console.log(finalOutPut)

  //==============================================================================================

// - How to check if an array contains any element of another array

     const studentIds = [1, 2,34, 5,6 ,7, 8]
     const passedIds = [1,2,3]

     let outPut = studentIds.some (ele => passedIds.includes(ele))
     console.log(outPut)
    