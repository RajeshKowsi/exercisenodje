const { fork } = require('child_process');

const mycChildProcess = fork('forkchildprocess.js')

mycChildProcess.send({hello : 'world'})


mycChildProcess.on('message', (msg) =>{
    console.log('Message from child  ', msg)
})

mycChildProcess.on('exit', (msg) =>{
    console.log('child process terminated ', msg)
})

console.log("checking processid of child process : "+process.pid)





