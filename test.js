//   Find prime number in List

var min = 2;
var max = 12;
var arr = []

for (i = min; i < max; i++) {
  flag = 0
  for (j = 2; j < i; j++) {
    if (i % j == 0) {
      flag = 1
      break
    }
  }

  if (i > 1 && flag == 0) {
    arr.push(i)
  }
}
console.log(arr);

//========================================================================================================================================================================

//Sort array ......................
var arr = [5, 4, 7, 9, 98, 3, 34, 12, 16]
arr = arr.sort((a, b) => a - b)
console.log(`Sortd values   --   ${arr}`)

//sort String.......................
var arr1 = ["Rrr", "chr", "Ba", "Aa", "DD"]
arr1 = arr1.sort()
console.log(`Sortd string values   --   ${arr1}`)


//===========================================================================================================================================================================

//Remove even number from arr and multiple by 2
// even numbers -- 2,4,6,8, etc..
// odd  numbers -- 1,3,5,7, etc..

var arr3 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
var evenNumberPick = (n) => n % 2 !== 0

arr3 = arr3.filter(evenNumberPick).map(n => n * 2)
console.log(`Odd number   --   ${arr3}`)

//=========================================================================================================================================================================

//Palindrom or not 

var data = "madam"
var result;
var len = data.length
for (i = 0; i < len / 2; i++) {

  console.log(`${data[i]}    --    ${data[len - 1 - i]}`)

  if (data[i] == data[len - 1 - i]) {
    result = " Polindrom!"
  } else {
    result = "Not  Polindrom!"
    console.log(result)
   break
  }
}

console.log(` ----${result}`)
//=======================================================================================================================================================================           
//remove duplications

const arr_ = [1, 1, 2, 3, 3, 4, 4, 4, 5, 5];

const arr_1 = [...new Set(arr_)]
console.log(`Remove Duplications -- ${arr_1}`)

//==========================================================================================================================================================================


// Testing shuffleing
const arr_s = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const shuffleingValues = arr_s.sort(() => Math.random() - 0.5)
console.log(`--${shuffleingValues}--`)

//=========================================================================================================================================================================

//-- closer is a combination of function bundle together with reference of its surrounding state (the lexical Environment). 
//-- closer its give access to another function scope as a inner function
//-- In the  jave script closer will crated at the time of function created. 

function x() {
  var a = 7;
  function y() {
    console.log(a)
  }
  y()
}

x()

//=======================================================================================================================================================================
// find the smallet and largest number

var arr_sort = [3, 5, 1, 4, 7, 51, 8, 6, 9, 23];
//var arr_sort = [54,4,2,5,1,22,9,3,11,15,10];

var len = arr_sort.length
// sorting
for (i = 0; i < len; i++) {
  if (arr_sort[i] > arr_sort[i + 1]) {
    var temp = arr_sort[i];

    arr_sort[i] = arr_sort[i + 1]
    arr_sort[i + 1] = temp
  }
}
console.log(` Sorted array ${arr_sort}`)
console.log(` Sorted array min ${arr_sort[0]}`)
console.log(` Sorted array max ${arr_sort[len - 1]}`)

//try with one for loop smallet and largest number

var nums = [55, 12, 11, 25, 666, 73, 13];
var min = nums[0]
var max = nums[0]
for (i = 1; i < nums.length; i++) {
  if (nums[i] > max) {
    max = nums[i]
  } else if (nums[i] < min) {
    min = nums[i]
  }
}

console.log("min  - ", min)

console.log("max   - ", max)


//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Print the Statement up to n, If the number is Fibonacci and Even print,
//If the number is only Fibonacci print , If the number is even print even

//fibonacci means like 0,1,1,2,3,5,8,13

n = 20
function fibonacci(n) {
  first = 0; second = 1;
  var arr_f = [];

  for (i = 0; i < n; i++) {
    if (i <= 1) {
      nextitem = i
      arr_f.push(nextitem)
    }
    else {
      nextitem = 0;
      nextitem = first + second;
      first = second
      second = nextitem
      arr_f.push(nextitem)
    }

  }

  console.log(` arr_f -> ${arr_f}`)
}

fibonacci(n)

/////////////////////////////////////=====================================================================================================================================

//How many Divisible sum pairs
Input:
//K=5
//A=[1,2,3,4,5,6,7]
//Output: 4

var nums = [1,2,3,4,5,6,7]
count=0;
for(i=0;i<nums.length; i++){
    
    for(j=i+1; j<nums.length; j++){
        if((nums[i]+nums[j]) % 5 == 0){
            count++
        }
    }
}

console.log(count)

//=======================================================================
console.log("---------------")
var numberString = "Hello User, Hello User, Hello User"

var regExpattern = /er/g;

if (regExpattern.test(numberString) == true) {
  var response = regExpattern.lastIndex
  console.log(response)
}
//==========================================================================================================================================================================
// Replacement 
const string = 'Hello hello hello';

// performing a replacement
const result1 = string.replace(/hello/, 'world');
console.log(result1); // Hello world hello

// performing global replacement
const result2 = string.replace(/hello/g, 'world');
console.log(result2); // Hello world world

// performing case-insensitive replacement
const result3 = string.replace(/hello/ig, 'world');
console.log(result3); // world hello hello

// performing global case-insensitive replacement
const result4 = string.replace(/hello/gi, 'world');
console.log(result4); // world world world

//=========================================================================

Sorting

var arr = [55, 12, 11,1, 25, 7,73, 13];
len = arr.length

var done = false
while(!done){
  done = true
for(i=0; i<len; i++){
      if(arr[i]> arr[i+1])
      {
        done = false
        temp = arr[i]
        arr[i] = arr[i+1]
        arr[i+1] = temp
        
      }
}
}

console.log(arr);

//=========================================================================


