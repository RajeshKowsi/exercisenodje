
const testfun = async function(){

}

async function forLoop(){
    const names = ["AAAAA", "BBBB", "CCCCCC"]

    for(i=0; i<names.length; i++){
        console.log(`  for loop ${names[i]}`)
    }

    for(const index in names){
        console.log(` In for loop ${names[index]}`)
    }

    for(const name of names){
        console.log(` Of for loop ${name}`)
    }

    names.forEach(async function(name){
        console.log(` forEach for loop ${name}`)
    })

}

forLoop()



