// for(i=0; i<3; i++){

//     setTimeout(()=>
//     console.log(i)
//     ,3*1000)
// }

// OutPut:
// 3
// 3
// 3
//=================================================

   let i=0    // here we define 2 time i value even it will not give compilation error. Because each for loop have differnt scope

for(leti=0; i<3; i++){

    setTimeout(()=>
    console.log(i)
    ,3*1000)
}

OutPut:
0
1
2
//============================================================
//// for(let i=0; i<3; i++){

//     setTimeout(()=>
//     console.log(i)
//     ,3*1000)
// }

// OutPut:
// 0
// 1
// 2
//=================================================

//=================================================

// let loopTest = () =>{

//     {
//         let a=10
//         var b=5
//     }
//      console.log(b)   //5
//      console.log(a)   // a is not defind

// }

// loopTest()
// console.log(b)  //b is not defind

//===========================================================================


// let printLoop = () =>{

//       {
//           var baz = 0
//           setTimeout (()=>
//           console.log(baz),1000
//           );
//      }

//        {
//         var baz = 1
//         setTimeout (()=>
//         console.log(baz),1000
//         );
//       }

//       {
//         var baz = 2
//         setTimeout (()=>
//         console.log(baz),1000
//         );
//       }


//       console.log(baz)
// }

// printLoop()

// //out put:
// 2
// 2
// 2
// 2

//===================================================================================



// let printLoop = () =>{

//     {
//         let baz = 0
//         setTimeout (()=>
//         console.log(baz),1000
//         );
//    }

//      {
//       let baz = 1
//       setTimeout (()=>
//       console.log(baz),1000
//       );
//     }

//     {
//       let baz = 2
//       setTimeout (()=>
//       console.log(baz),1000
//       );
//     }


// }

// printLoop()

// //out put:
// // 0
// // 1
// // 2

//===============================================

