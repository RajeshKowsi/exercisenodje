const Bull = require('bull')

const redisURI = 'redis://127.0.0.1'

const testQueue = new Bull('test', redisURI);

testQueue.process(() => {
  console.log('Bull job executes every minute')
})

testQueue.add({}, {
  repeat: {
    cron: '* * * * *'
  }
})

testQueue.add({}, {
  repeat: {
    cron: '* * * * *'
  }
})

testQueue.add({}, {
  repeat: {
    cron: '* * * * *'
  }
})

testQueue.add({}, {
  repeat: {
    cron: '* * * * *'
  }
})

//https://medium.com/geekculture/cron-jobs-in-node-js-8df170445588