////=========================================CALL BACK WITH FUNCTION CALLING===============

// first(2, (firstResult, err) => {
//     if (!err) {
//         second(firstResult, (secondResult, err) => {
//             if (!err) {
//                 third(secondResult, (thirdResult, err) => {
//                     if (!err) {
//                         console.log(`--------${thirdResult}---------`)
//                     }
//                 })
//             }
//         })

//     }
// })


// function first(value, callback) {
//     callback(value + 2)
// }

// function second(value, callback) {
//     callback(value + 2)
// }


// function third(value, callback) {
//     callback(value + 2)
// }


///////////////////////////////////////////////////////////////////////////////////////////////////
////=========================================PROMISE WITH FUNCTION CALLING===============

// let firstt = function (value) {
//     return (value + 2)
// }

// let secondt =  (value) => {return(value + 2)}


// let thirdt =  (value) => {
//     return(value + 2)
// }


// let pp = new Promise((reslove, reject) => reslove(2))

// pp.then(firstt).then(secondt).then(thirdt).then((response) => {
//     console.log(`===========${response}===========`)
// })


//=============================================================================================================================================
                    // promise


const myPromise = new Promise((reslove, reject) =>{
    const condition = true
    if(condition){
        setTimeout(()=>{
                   reslove("Promise Solved!")
        },3000)
    }else{
        reject("Not Solved Error!")
    }
});



// var execute =() =>{
//     myPromise.then((result) =>{
//         console.log(`----------${result}----------`)
//    }).catch((error) => console.log(` ------${error}-------`))
// }

// execute()

//===========================================================================================================================================
                /// function chain
const hellopromise = () =>
{
    return  new Promise((resolve, reject) =>{

        resolve("Hell Test!!")

    })
}

myPromise.then(hellopromise).then((Result) =>{
    console.log(Result)
}).catch((error) =>{
    console.log(error)
})