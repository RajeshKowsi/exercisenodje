//  //*********************************Example 1 with promise wait************ */
console.log('Person 1')
console.log('Person 2')


let gotticket = new Promise ((resolve, reject) =>
 setTimeout(()=> resolve("Ticket"),3000));

 let needPopCron = gotticket.then((t)=> {
                 console.log("got ticket")
                 console.log("feel so hungry")
                 return new Promise((resolve, reject) => resolve(`${t} PopCron`))
    });

   let butterGot = needPopCron.then((p) => {
        console.log("got popcron")
        console.log("need butter")
        return new Promise((resolve, reject) => resolve(`${p} butter`))
    })

    butterGot.then((b) => console.log(`${b}`))



console.log('Person 4')
console.log('Person 5')

//////////////////////////////////////////////////////////////////////////////////////
 //*********************************Example 2 with async wait************ */

// console.log('Person 1')
// console.log('Person 2')

// let proMovie = async() =>{

//       let wifeTakenTicket = new Promise((reslove, reject) => setTimeout(() => reslove("Ticket"),3000))
//       let getPopCron = new Promise((reslove, reject) =>  reslove(" PopCron"))
//       let getbutter = new Promise((reslove, reject) => reslove(" Butter"))
//       let getZero= new Promise((reslove, reject) => reslove(" Zero"))

//      let ticket = await wifeTakenTicket
//      console.log(`wife got ${ticket}`)
//      let popCron = await getPopCron
//      console.log(`wife got ${popCron}`)
//      let butter = await getbutter
//      console.log(`wife got ${butter}`)
//      let zero = await getZero
//      console.log(`wife got ${zero}`)

//      let [tickets, popCrons, butters, zeros] = await Promise.all([wifeTakenTicket,getPopCron,getbutter, getZero ])
//      console.log( `${tickets} , ${popCrons},${butters} ,${zeros}  `)

//      return wifeTakenTicket


// }
// proMovie().then((t) => console.log(t))

// console.log('Person 3')
// console.log('Person 4')

///===========================================================================================================================================
 //*********************************Example 3 with async wait with promise.all************ */

console.log('Person 1')
console.log('Person 2')

let proMovie = async() =>{

      let wifeTakenTicket = new Promise((reslove, reject) => setTimeout(() => reslove("Ticket"),3000))
      let getPopCron = new Promise((reslove, reject) =>  reslove(" PopCron"))
      let getbutter = new Promise((reslove, reject) => reslove(" Butter"))
      let getZero= new Promise((reslove, reject) => reslove(" Zero"))

    
     let [tickets, popCrons, butters, zeros] = await Promise.all([wifeTakenTicket,getPopCron,getbutter, getZero ])
     console.log( `${tickets} , ${popCrons},${butters} ,${zeros}  `)

     return wifeTakenTicket


}
proMovie().then((t) => console.log(t))

console.log('Person 3')
console.log('Person 4')

// //=========================================================================================================================================================================
// //*********************************Example 4 with async wait with errror handle************ */


// console.log('Person 1')
// console.log('Person 2')

// let proMovies = async() =>{

//       let wifeTakenTicket = new Promise((reslove, reject) => setTimeout(() => reject("Ticket"),3000))

//       let ticket 
//       try {
//           ticket = await wifeTakenTicket
//       } catch (error) {
//         ticket = 'Sad face'
//       }


//      return ticket


// }
// proMovies().then((t) => console.log(t))

// console.log('Person 3')
// console.log('Person 4')
