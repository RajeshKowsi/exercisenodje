const express = require("express")
const cluster = require('cluster')
const totalcpus = require("os").cpus().length
if(cluster.isMaster){
console.log(` Total CPU is ${totalcpus}`)
console.log(` Master ${process.pid} is running`)

         for(let i =0; i< totalcpus; i++){
             cluster.fork();
         }

         cluster.on('exit', (worker, code, signal) =>{
            console.log(` Master ${worker.process.pid} died`)
            console.log(` Lets fork another workers`)
            cluster.fork();
         })

}else{

const app =express()
const PORT = 3000
console.log(` worker ${process.pid} is running`)
app.get('/', (req, res)=>{
    res.send('Hello World')
})

app.get('/api/:n', function(req, res){
    let n = parseInt(req.params.n);
    let count =0;
    if(n >500000000) n=5000000000;

    for(let i =0; i<=n; i++){
        count +=i;
    }

    res.send(`Final count is ${count} `)
})


app.listen(PORT, () =>{
    console.log("Test")
})

}

//https://ruthvikrokz.medium.com/cluster-module-node-js-f1508bcb196a